function A = A_generateTerminalGroundingFunction(E,Ek,n)
    arguments (Input)
        E (2,4) cell
        Ek (4,4)
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4  % probably will always be 4 since there are 4 "contacts" (in/out...)
    end
    arguments (Output)
        A {mustBeVector(A), mustBeMember(A,[0 1])}
    end
    A = zeros(n,1);
    allZeroOrOne = randi([0 1],1);
    if Ek == E{1,1}
        if allZeroOrOne == 1 %% add all ones
            for i = 1 : n
                A(i) = 1;
            end
        end % else keep A with all zeroes
    elseif Ek == E{1,2}
        if allZeroOrOne == 1 % add 1,2,3 with one or zero (default is zero)
            for i = 1 : n-1
                A(i) = 1;
            end
        end
        A(n) = randi([0 1],1); % add zero or one to the last index
    elseif Ek == E{1,3}
        if allZeroOrOne == 1 % add 1,3,4 with one or zero (default is zero)
            A(1) = 1;
            A(3) = 1;
            A(4) = 1;
        end
        A(2) = randi([0 1],1); % add zero or one 
    elseif Ek == E{1,4}
        if allZeroOrOne == 1 % add 1,3 with one or zero (default is zero)
            A(1) = 1;
            A(3) = 1;
        end
        A(2) = randi([0 1],1); % add zero or one to the last index
        A(4) = randi([0 1],1); % add zero or one to the last index
    elseif Ek == E{2,1}
        if allZeroOrOne == 1 % add 2,3,4 with one or zero (default is zero)
            A(2) = 1;
            A(3) = 1;
            A(4) = 1;
        end
        A(1) = randi([0 1],1); % add zero or one to the last index
    elseif Ek == E{2,2}
        if allZeroOrOne == 1 % add 2,4 with one or zero (default is zero)
            A(2) = 1;
            A(4) = 1;
        end
        A(1) = randi([0 1],1); % add zero or one to the last index
        A(3) = randi([0 1],1); % add zero or one to the last index
    elseif Ek == E{2,3}
        if allZeroOrOne == 1 % add 1,3 with one or zero (default is zero)
            A(1) = 1;
            A(3) = 1;
        end
        zeroOrOneSecondPart = randi([0 1],1); % add 2,4 with one or zero (default is zero)
        if zeroOrOneSecondPart == 1 
            A(2) = 1; % add zero or one to the last index
            A(4) = 1; % add zero or one to the last index
        end
    elseif Ek == E{2,4}
        if allZeroOrOne == 1 % add 1,2,4 with one or zero (default is zero)
            A(1) = 1;
            A(2) = 1;
            A(4) = 1;
        end
        A(3) = randi([0 1],1); % add zero or one to the last index
    end
end