function individualIndex = rouletteWheelSelection(cumulativeFitness, populationSize)
    arguments (Input)
        cumulativeFitness (1,1) int32 {mustBeGreaterThanOrEqual(cumulativeFitness,0)}
        populationSize (1,1) int32 {mustBeGreaterThanOrEqual(populationSize,10)} 
    end
    arguments (Output)
        individualIndex (1,1) int32 {mustBeGreaterThan(individualIndex,0)}
    end
    randomSelectionPoint = randi(cumulativeFitness);
    sum = 0;
    individualIndex = 1; %% this should always be replaced with the selected individual
    for j = populationSize : -1 : 1
        sum = sum + j;
        if sum >= randomSelectionPoint
            break;
        end
        individualIndex = individualIndex + 1;
    end
end
