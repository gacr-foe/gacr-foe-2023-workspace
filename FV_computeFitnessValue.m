function FV = FV_computeFitnessValue(E, CCh, CCh_B, PCh, n, phaseRange, frequencyRange, frequencyPoints, r, c)
    arguments (Input)
        E (2,4) cell
        CCh (:,2) cell
        CCh_B {mustBeMember(CCh_B,[0 1])}
        PCh (2,:)
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} % number of nodes in the solution
        phaseRange (1,2)
        frequencyRange (1,2)
        frequencyPoints (1,1) int32 {mustBeGreaterThanOrEqual(frequencyPoints,1)}
        r (1,1) {mustBeReal}
        c (1,1) {mustBeReal}
    end
    arguments (Output)
        FV (1,1) int32 {mustBeGreaterThanOrEqual(FV,0)}
    end

    % function fit = calcFit(maxPhase,minPhase,N,r,c,L1,L2,L3,L4,E1,E2,E3,A1,A2,A3,B,startfreq,endfreq,freqpoints)
    minPhase = phaseRange(1); % minimum allowed phase in degrees
    maxPhase = phaseRange(2); %37; % maximum allowed phase in degrees
    N = PCh(1,1); % ratio of bottom and top layer resistance
    % zadávat jako reálnou kapacitu čtverce na čipu, kde jeho strana = předpokládaná realizovatelná šířka vrstvy, např. 0,4 um
    L1 = PCh(2,1); % normalized (dimensionless) length of segment (normalized width = 1)
    L2 = PCh(2,2);
    L3 = PCh(2,3);
    L4 = PCh(2,4);
    % omezit poměr délek nejdelšího a nejkratšího úseku sekcí např. na 10:1,
    % možná i stanovit min. délku (větší než 1)

    E1 = CCh{1,1}; % E1
    A1 = CCh{1,2}; % A1 % [0;1;0;0];
    E2 = CCh{2,1}; % E2
    A2 = CCh{2,2}; % A2 [0;0;0;0];
    E3 = CCh{3,1}; % E3
    A3 = CCh{3,2}; % A3 [0;0;0;0];
    B = CCh_B;
    startfreq = frequencyRange(1); % 1e3; % Hz (frekvenční rozsah by měl být mezi 1 kHz a 10 MHz)
    endfreq = frequencyRange(2); %10e6; % Hz
    freqpoints = frequencyPoints;
    
    w = reshape(logspace(log10(2*pi*startfreq),log10(2*pi*endfreq),freqpoints),1,1,[]); % normalized frequency array
    th1 = sqrt(1i*w*r*c*(1+N))*L1;
    Yseg1 = (1/((1+N)*r*L1))*[th1./tanh(th1)+N -th1./sinh(th1)-N th1./sinh(th1)-1 1-th1./tanh(th1);
    -th1./sinh(th1)-N th1./tanh(th1)+N 1-th1./tanh(th1) th1./sinh(th1)-1;
    th1./sinh(th1)-1 1-th1./tanh(th1) th1./tanh(th1)+1/N -th1./sinh(th1)-1/N;
    1-th1./tanh(th1) th1./sinh(th1)-1 -th1./sinh(th1)-1/N th1./tanh(th1)+1/N];
    
    th2 = sqrt(1i*w*r*c*(1+N))*L2;
    Yseg2 = (1/((1+N)*r*L2))*[th2./tanh(th2)+N -th2./sinh(th2)-N th2./sinh(th2)-1 1-th2./tanh(th2);
    -th2./sinh(th2)-N th2./tanh(th2)+N 1-th2./tanh(th2) th2./sinh(th2)-1;
    th2./sinh(th2)-1 1-th2./tanh(th2) th2./tanh(th2)+1/N -th2./sinh(th2)-1/N;
    1-th2./tanh(th2) th2./sinh(th2)-1 -th2./sinh(th2)-1/N th2./tanh(th2)+1/N];
    
    th3 = sqrt(1i*w*r*c*(1+N))*L3;
    Yseg3 = (1/((1+N)*r*L3))*[th3./tanh(th3)+N -th3./sinh(th3)-N th3./sinh(th3)-1 1-th3./tanh(th3);
    -th3./sinh(th3)-N th3./tanh(th3)+N 1-th3./tanh(th3) th3./sinh(th3)-1;
    th3./sinh(th3)-1 1-th3./tanh(th3) th3./tanh(th3)+1/N -th3./sinh(th3)-1/N;
    1-th3./tanh(th3) th3./sinh(th3)-1 -th3./sinh(th3)-1/N th3./tanh(th3)+1/N];
    
    th4 = sqrt(1i*w*r*c*(1+N))*L4;
    Yseg4 = (1/((1+N)*r*L4))*[th4./tanh(th4)+N -th4./sinh(th4)-N th4./sinh(th4)-1 1-th4./tanh(th4);
    -th4./sinh(th4)-N th4./tanh(th4)+N 1-th4./tanh(th4) th4./sinh(th4)-1;
    th4./sinh(th4)-1 1-th4./tanh(th4) th4./tanh(th4)+1/N -th4./sinh(th4)-1/N;
    1-th4./tanh(th4) th4./sinh(th4)-1 -th4./sinh(th4)-1/N th4./tanh(th4)+1/N];
    
    Yglob = zeros(16,16,freqpoints);
    % "stamping" of Yglob matrix by the Yseg matrices of partial segments
    Yglob([1 2 5 6],[1 2 5 6],:) = Yseg1([1 4 2 3],[1 4 2 3],:);
    Yglob([7 8 9 10],[7 8 9 10],:) = Yseg2([1 4 2 3],[1 4 2 3],:);
    Yglob([11 12 13 14],[11 12 13 14],:) = Yseg3([1 4 2 3],[1 4 2 3],:);
    Yglob([15 16 3 4],[15 16 3 4],:) = Yseg4([1 4 2 3],[1 4 2 3],:);
    
    % operations with Yglob based on the connections between the segments
    % (three boundaries inside the structure)
    % boundary nodes 13-16
    nA3 = (nonzeros(A3.*[15;16;13;14]))'; % numbers of grounded nodes
    if all(E3 == E{1,1})
        nE3 = [13 14 15 16]; % numbers of interconnected nodes
        if isempty(nA3) % no grounded nodes from the group 13-16
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(14,:,:)+Yglob(15,:,:)+Yglob(16,:,:); % row 13 = sum of rows 13-16
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,14,:)+Yglob(:,15,:)+Yglob(:,16,:); % column 13 = sum of columns 13-16
            Yglob([14 15 16],:,:) = []; % delete rows 14-16
            Yglob(:,[14 15 16],:) = []; % delete columns 14-16
        else % any grounded node from the group 13-16
            Yglob([13 14 15 16],:,:) = []; % delete rows and columns 13-16
            Yglob(:,[13 14 15 16],:) = [];
        end
    elseif all(E3 == E{1,2})
        nE3 = [13 15 16];
        if isempty(intersect(nE3,nA3))
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(15,:,:)+Yglob(16,:,:);
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,15,:)+Yglob(:,16,:);
            Yglob([15 16 nA3],:,:) = [];
            Yglob(:,[15 16 nA3],:) = [];
        else
            Yglob(unique([13 15 16 nA3]),:,:) = [];
            Yglob(:,unique([13 15 16 nA3]),:) = [];
        end
    elseif all(E3 == E{1,3})
        nE3 = [13 14 15];
        if isempty(intersect(nE3,nA3))
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(14,:,:)+Yglob(15,:,:);
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,14,:)+Yglob(:,15,:);
            Yglob([14 15 nA3],:,:) = [];
            Yglob(:,[14 15 nA3],:) = [];
        else
            Yglob(unique([13 14 15 nA3]),:,:) = [];
            Yglob(:,unique([13 14 15 nA3]),:) = [];
        end    
    elseif all(E3 == E{1,4})
        nE3 = [13 15];
        if isempty(intersect(nE3,nA3))
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(15,:,:);
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,15,:);
            Yglob([15 nA3],:,:) = [];
            Yglob(:,[15 nA3],:) = [];
        else
            Yglob(unique([13 15 nA3]),:,:) = [];
            Yglob(:,unique([13 15 nA3]),:) = [];
        end
    elseif all(E3 == E{2,1})
        nE3 = [13 14 16];
        if isempty(intersect(nE3,nA3))
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(14,:,:)+Yglob(16,:,:);
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,14,:)+Yglob(:,16,:);
            Yglob([14 16 nA3],:,:) = [];
            Yglob(:,[14 16 nA3],:) = [];
        else
            Yglob(unique([13 14 16 nA3]),:,:) = [];
            Yglob(:,unique([13 14 16 nA3]),:) = [];
        end        
    elseif all(E3 == E{2,2})
        nE3 = [14 16];
        if isempty(intersect(nE3,nA3))
            Yglob(14,:,:) = Yglob(14,:,:)+Yglob(16,:,:);
            Yglob(:,14,:) = Yglob(:,14,:)+Yglob(:,16,:);
            Yglob([16 nA3],:,:) = [];
            Yglob(:,[16 nA3],:) = [];
        else
            Yglob(unique([14 16 nA3]),:,:) = [];
            Yglob(:,unique([14 16 nA3]),:) = [];
        end    
    elseif all(E3 == E{2,3})
        nE3a = [13 15];
        nE3b = [14 16];
        if isempty(nA3)
            Yglob([13 14],:,:) = Yglob([13 14],:,:)+Yglob([15 16],:,:);
            Yglob(:,[13 14],:) = Yglob(:,[13 14],:)+Yglob(:,[15 16],:);
            Yglob([15 16],:,:) = [];
            Yglob(:,[15 16],:) = [];
        elseif (~isempty(intersect(nE3a,nA3))) && (isempty(intersect(nE3b,nA3)))
            Yglob(14,:,:) = Yglob(14,:,:)+Yglob(16,:,:);
            Yglob(:,14,:) = Yglob(:,14,:)+Yglob(:,16,:);
            Yglob([13 15 16],:,:) = [];
            Yglob(:,[13 15 16],:) = [];
        elseif (isempty(intersect(nE3a,nA3))) && (~isempty(intersect(nE3b,nA3)))
            Yglob(13,:,:) = Yglob(13,:,:)+Yglob(15,:,:);
            Yglob(:,13,:) = Yglob(:,13,:)+Yglob(:,15,:);
            Yglob([14 15 16],:,:) = [];
            Yglob(:,[14 15 16],:) = [];
        else
            Yglob([13 14 15 16],:,:) = [];
            Yglob(:,[13 14 15 16],:) = [];
        end
    elseif all(E3 == E{2,4})
        nE3 = [14 15 16]; % cisla zkratovanych uzlu
        if isempty(intersect(nE3,nA3))
            Yglob(14,:,:) = Yglob(14,:,:)+Yglob(15,:,:)+Yglob(16,:,:);
            Yglob(:,14,:) = Yglob(:,14,:)+Yglob(:,15,:)+Yglob(:,16,:);
            Yglob([15 16 nA3],:,:) = [];
            Yglob(:,[15 16 nA3],:) = [];
        else
            Yglob(unique([14 15 16 nA3]),:,:) = [];
            Yglob(:,unique([14 15 16 nA3]),:) = [];
        end            
    end
    
    % boundary nodes 9-12
    nA2 = (nonzeros(A2.*[11;12;9;10]))';
    if all(E2 == E{1,1})
        nE2 = [9 10 11 12];
        if isempty(nA2)
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(10,:,:)+Yglob(11,:,:)+Yglob(12,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,10,:)+Yglob(:,11,:)+Yglob(:,12,:);
            Yglob([10 11 12],:,:) = [];
            Yglob(:,[10 11 12],:) = [];
        else
            Yglob([9 10 11 12],:,:) = [];
            Yglob(:,[9 10 11 12],:) = [];
        end
    elseif all(E2 == E{1,2})
        nE2 = [9 11 12];
        if isempty(intersect(nE2,nA2))
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(11,:,:)+Yglob(12,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,11,:)+Yglob(:,12,:);
            Yglob([11 12 nA2],:,:) = [];
            Yglob(:,[11 12 nA2],:) = [];
        else
            Yglob(unique([9 11 12 nA2]),:,:) = [];
            Yglob(:,unique([9 11 12 nA2]),:) = [];
        end
    elseif all(E2 == E{1,3})
        nE2 = [9 10 11];
        if isempty(intersect(nE2,nA2))
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(10,:,:)+Yglob(11,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,10,:)+Yglob(:,11,:);
            Yglob([10 11 nA2],:,:) = [];
            Yglob(:,[10 11 nA2],:) = [];
        else
            Yglob(unique([9 10 11 nA2]),:,:) = [];
            Yglob(:,unique([9 10 11 nA2]),:) = [];
        end    
    elseif all(E2 == E{1,4})
        nE2 = [9 11];
        if isempty(intersect(nE2,nA2))
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(11,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,11,:);
            Yglob([11 nA2],:,:) = [];
            Yglob(:,[11 nA2],:) = [];
        else
            Yglob(unique([9 11 nA2]),:,:) = [];
            Yglob(:,unique([9 11 nA2]),:) = [];
        end
    elseif all(E2 == E{2,1})
        nE2 = [9 10 12];
        if isempty(intersect(nE2,nA2))
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(10,:,:)+Yglob(12,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,10,:)+Yglob(:,12,:);
            Yglob([10 12 nA2],:,:) = [];
            Yglob(:,[10 12 nA2],:) = [];
        else
            Yglob(unique([9 10 12 nA2]),:,:) = [];
            Yglob(:,unique([9 10 12 nA2]),:) = [];
        end        
    elseif all(E2 == E{2,2})
        nE2 = [10 12];
        if isempty(intersect(nE2,nA2))
            Yglob(10,:,:) = Yglob(10,:,:)+Yglob(12,:,:);
            Yglob(:,10,:) = Yglob(:,10,:)+Yglob(:,12,:);
            Yglob([12 nA2],:,:) = [];
            Yglob(:,[12 nA2],:) = [];
        else
            Yglob(unique([10 12 nA2]),:,:) = [];
            Yglob(:,unique([10 12 nA2]),:) = [];
        end    
    elseif all(E2 == E{2,3})
        nE2a = [9 11];
        nE2b = [10 12];
        if isempty(nA2)
            Yglob([9 10],:,:) = Yglob([9 10],:,:)+Yglob([11 12],:,:);
            Yglob(:,[9 10],:) = Yglob(:,[9 10],:)+Yglob(:,[11 12],:);
            Yglob([11 12],:,:) = [];
            Yglob(:,[11 12],:) = [];
        elseif (~isempty(intersect(nE2a,nA2))) && (isempty(intersect(nE2b,nA2)))
            Yglob(10,:,:) = Yglob(10,:,:)+Yglob(12,:,:);
            Yglob(:,10,:) = Yglob(:,10,:)+Yglob(:,12,:);
            Yglob([9 11 12],:,:) = [];
            Yglob(:,[9 11 12],:) = [];
        elseif (isempty(intersect(nE2a,nA2))) && (~isempty(intersect(nE2b,nA2)))
            Yglob(9,:,:) = Yglob(9,:,:)+Yglob(11,:,:);
            Yglob(:,9,:) = Yglob(:,9,:)+Yglob(:,11,:);
            Yglob([10 11 12],:,:) = [];
            Yglob(:,[10 11 12],:) = [];
        else
            Yglob([9 10 11 12],:,:) = [];
            Yglob(:,[9 10 11 12],:) = [];
        end
    elseif all(E2 == E{2,4})
        nE2 = [10 11 12];
        if isempty(intersect(nE2,nA2))
            Yglob(10,:,:) = Yglob(10,:,:)+Yglob(11,:,:)+Yglob(12,:,:);
            Yglob(:,10,:) = Yglob(:,10,:)+Yglob(:,11,:)+Yglob(:,12,:);
            Yglob([11 12 nA2],:,:) = [];
            Yglob(:,[11 12 nA2],:) = [];
        else
            Yglob(unique([10 11 12 nA2]),:,:) = [];
            Yglob(:,unique([10 11 12 nA2]),:) = [];
        end            
    end
    % boundary nodes 5-8
    nA1 = (nonzeros(A1.*[7;8;5;6]))';
    if all(E1 == E{1,1})
        nE1 = [5 6 7 8];
        if isempty(nA1)
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(6,:,:)+Yglob(7,:,:)+Yglob(8,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,6,:)+Yglob(:,7,:)+Yglob(:,8,:);
            Yglob([6 7 8],:,:) = [];
            Yglob(:,[6 7 8],:) = [];
        else
            Yglob([5 6 7 8],:,:) = [];
            Yglob(:,[5 6 7 8],:) = [];
        end
    elseif all(E1 == E{1,2})
        nE1 = [5 7 8];
        if isempty(intersect(nE1,nA1))
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(7,:,:)+Yglob(8,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,7,:)+Yglob(:,8,:);
            Yglob([7 8 nA1],:,:) = [];
            Yglob(:,[7 8 nA1],:) = [];
        else
            Yglob(unique([5 7 8 nA1]),:,:) = [];
            Yglob(:,unique([5 7 8 nA1]),:) = [];
        end
    elseif all(E1 == E{1,3})
        nE1 = [5 6 7];
        if isempty(intersect(nE1,nA1))
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(6,:,:)+Yglob(7,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,6,:)+Yglob(:,7,:);
            Yglob([6 7 nA1],:,:) = [];
            Yglob(:,[6 7 nA1],:) = [];
        else
            Yglob(unique([5 6 7 nA1]),:,:) = [];
            Yglob(:,unique([5 6 7 nA1]),:) = [];
        end    
    elseif all(E1 == E{1,4})
        nE1 = [5 7];
        if isempty(intersect(nE1,nA1))
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(7,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,7,:);
            Yglob([7 nA1],:,:) = [];
            Yglob(:,[7 nA1],:) = [];
        else
            Yglob(unique([5 7 nA1]),:,:) = [];
            Yglob(:,unique([5 7 nA1]),:) = [];
        end
    elseif all(E1 == E{2,1})
        nE1 = [5 6 8];
        if isempty(intersect(nE1,nA1))
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(6,:,:)+Yglob(8,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,6,:)+Yglob(:,8,:);
            Yglob([6 8 nA1],:,:) = [];
            Yglob(:,[6 8 nA1],:) = [];
        else
            Yglob(unique([5 6 8 nA1]),:,:) = [];
            Yglob(:,unique([5 6 8 nA1]),:) = [];
        end        
    elseif all(E1 == E{2,2})
        nE1 = [6 8];
        if isempty(intersect(nE1,nA1))
            Yglob(6,:,:) = Yglob(6,:,:)+Yglob(8,:,:);
            Yglob(:,6,:) = Yglob(:,6,:)+Yglob(:,8,:);
            Yglob([8 nA1],:,:) = [];
            Yglob(:,[8 nA1],:) = [];
        else
            Yglob(unique([6 8 nA1]),:,:) = [];
            Yglob(:,unique([6 8 nA1]),:) = [];
        end    
    elseif all(E1 == E{2,3})
        nE1a = [5 7];
        nE1b = [6 8];
        if isempty(nA1)
            Yglob([5 6],:,:) = Yglob([5 6],:,:)+Yglob([7 8],:,:);
            Yglob(:,[5 6],:) = Yglob(:,[5 6],:)+Yglob(:,[7 8],:);
            Yglob([7 8],:,:) = [];
            Yglob(:,[7 8],:) = [];
        elseif (~isempty(intersect(nE1a,nA1))) && (isempty(intersect(nE1b,nA1)))
            Yglob(6,:,:) = Yglob(6,:,:)+Yglob(8,:,:);
            Yglob(:,6,:) = Yglob(:,6,:)+Yglob(:,8,:);
            Yglob([5 7 8],:,:) = [];
            Yglob(:,[5 7 8],:) = [];
        elseif (isempty(intersect(nE1a,nA1))) && (~isempty(intersect(nE1b,nA1)))
            Yglob(5,:,:) = Yglob(5,:,:)+Yglob(7,:,:);
            Yglob(:,5,:) = Yglob(:,5,:)+Yglob(:,7,:);
            Yglob([6 7 8],:,:) = [];
            Yglob(:,[6 7 8],:) = [];
        else
            Yglob([5 6 7 8],:,:) = [];
            Yglob(:,[5 6 7 8],:) = [];
        end
    elseif all(E1 == E{2,4})
        nE1 = [6 7 8];
        if isempty(intersect(nE1,nA1))
            Yglob(6,:,:) = Yglob(6,:,:)+Yglob(7,:,:)+Yglob(8,:,:);
            Yglob(:,6,:) = Yglob(:,6,:)+Yglob(:,7,:)+Yglob(:,8,:);
            Yglob([7 8 nA1],:,:) = [];
            Yglob(:,[7 8 nA1],:) = [];
        else
            Yglob(unique([6 7 8 nA1]),:,:) = [];
            Yglob(:,unique([6 7 8 nA1]),:) = [];
        end            
    end
    % end of operations with Yglob based on the connections between the segments
    
    % operations with Yglob based on the connections at the outside contacts of
    % all four segments based on B matrix
    if sum(B(4,:))==2 % if exists the interconnection con
        con = find(B(4,:)); % number of nodes interconnected by con
        Yglob(con(1),:,:) = Yglob(con(1),:,:)+Yglob(con(2),:,:);
        Yglob(:,con(1),:) = Yglob(:,con(1),:)+Yglob(:,con(2),:);
        Yglob(con(2),:,:) = []; % delete row and column with the second con contact
        Yglob(:,con(2),:) = [];
        B(:,con(2)) = []; % delete the column of B with the second con contact
    end
    gnd = find(B(2,:)); % numbers of grounded nodes
    Yglob(gnd,:,:) = []; % deleting rows and columns pertaining to grounded nodes
    Yglob(:,gnd,:) = [];
    B(:,gnd) = [];
    
    in = find(B(1,:)); % numbers of inpit nodes in
    if length(in)==2 % if there are 2 in nodes
        Yglob(in(1),:,:) = Yglob(in(1),:,:)+Yglob(in(2),:,:);
        Yglob(:,in(1),:) = Yglob(:,in(1),:)+Yglob(:,in(2),:);
        Yglob(in(2),:,:) = [];
        Yglob(:,in(2),:) = [];
    elseif length(in)==3 % if there are 3 in nodes
        Yglob(in(1),:,:) = Yglob(in(1),:,:)+Yglob(in(2),:,:)+Yglob(in(3),:,:);
        Yglob(:,in(1),:) = Yglob(:,in(1),:)+Yglob(:,in(2),:)+Yglob(:,in(3),:);
        Yglob(in(2:3),:,:) = [];
        Yglob(:,in(2:3),:) = [];
    end
    % input is at the node in(1)
    
    YsegNum = Yglob; % preparing matrix YsegNum for numerator of impedance function
    YsegNum(in(1),:,:) = []; % deleting input row and column to prepare submatrix for subdeterminant (minor) computation
    YsegNum(:,in(1),:) = [];
    Num = arrayfun(@(k) det(YsegNum(:,:,k)), 1:size(YsegNum,3)); % computation of determinant of each matrix in array
    Den = arrayfun(@(k) det(Yglob(:,:,k)), 1:size(Yglob,3));
    
    Y = Den./Num; % admittance function (Y=denominator/numerator bacause of Y=1/Z)
    magnitudeY = abs(Y);
    phaseY = 180/pi*angle(Y); % degrees
    
    fit = nnz((phaseY>=minPhase)&(phaseY<=maxPhase));

    %% compute fitness value for specific chromosome/individual from the population
    FV = fit;
end