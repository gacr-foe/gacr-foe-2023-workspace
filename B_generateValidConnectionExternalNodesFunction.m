function B = B_generateValidConnectionExternalNodesFunction(m,n) % m = rows; n = columns;
    arguments (Input)
        m (1,1) int32 {mustBeGreaterThanOrEqual(m,4)} = 4  % the number of rows equals to the number of possible states of external nodes (in, gnd, float, con)
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4  % the number of columns corresponds to the number of external nodes of EFI
    end
    arguments (Output)
        B {mustBeMember(B,[0 1])}
    end
    B = zeros(m,n);
    % below the "repair operations" are applied to provide valid external EFI switching scheme

    % Rule 1: randomly select if consider "float" or "con" node
    floatOrConn = randi([3 4],1);
    if floatOrConn == 3 % randomly add one "float" node and apply remaining rules
        addIn = randi(3); % one to three "in" nodes allowed
        for column = 1 : addIn
            B(1,randi(n)) = 1; % randomly add "in" node (might replace "1" with "1" which I do not see as an issue)
        end
        % Rule 2 there must be at least one "gnd" node
        zeroInIndexes = find(~B(1,:)); % find zero indexes of "in" node
        zeroIndexesCount = numel(zeroInIndexes);
        addGnd = randi(3);
        for column = 1 : addGnd
            B(2,zeroInIndexes(randi(zeroIndexesCount))) = 1; % fill-up at least one "gnd" node on column with zero "in" node
        end
        % Rule X the remaining zero columns fill-up with "float" node
        for col=1:n
            sumColumnValues = sum(B(:,col));
            if sumColumnValues == 0
                B(3, col) = 1; % add "1" to "float" node for zero columns
            end
        end
    else % randomly add two "con" node and apply remaining rules
        columnIndex1 = randi(n,1);
        columnIndex2 = columnIndex1;
        if n > 1
            while columnIndex1 == columnIndex2
                columnIndex2 = randi(n,1);
            end
        end
        B(floatOrConn, columnIndex1) = 1;
        B(floatOrConn, columnIndex2) = 1;
        % Rule X: add one "in" node
        zeroInIndexes = find(~B(4,:)); % find zero indexes of "in" node
        B(1,zeroInIndexes(randi(2))) = 1; % fill-up "gnd" node on column with zero "in" node
        % Rule X: add one "gnd" node where columns are zero
        indexOfZeroColumn = 0; %% should always be replaced or not used within IF statement
        for col=1:n
            sumColumnValues = sum(B(:,col));
            if sumColumnValues == 0
                indexOfZeroColumn = col;
            end
        end
        B(2,indexOfZeroColumn) = 1;
    end
    % Rule X: check if each column contains exactly one element ("1")
    % this is automatically achieved by the previous rules
end
