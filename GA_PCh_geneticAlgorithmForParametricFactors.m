function theBestParametricFactors = GA_PCh_geneticAlgorithmForParametricFactors(E,CCh, CCh_B, gaIterations, populationSize, selectionType, crossoverType, n, phaseRange, frequencyRange, frequencyPoints, r, c, N_range, L_range)
    arguments (Input)
        E (2,4) cell
        CCh (:,2) cell
        CCh_B {mustBeMember(CCh_B,[0 1])}
        gaIterations (1,1) int32 {mustBeGreaterThanOrEqual(gaIterations,10)} = 250  % default value
        populationSize (1,1) int32 {mustBeGreaterThanOrEqual(populationSize,10)} = 50  % default value of iterations of genetic algorithm
        selectionType {mustBeMember(selectionType,["rank","roulette","tournament"])} = 'rank' % default to rank selection
        crossoverType {mustBeMember(crossoverType,["one-point","two-point","uniform"])} = 'one-point' % default to one-point crossover
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4 %  number of nodes in the solution
        phaseRange (1,2) = [33 37]
        frequencyRange (1,2) = [0.0 20.0]
        frequencyPoints (1,1) int32 {mustBeGreaterThanOrEqual(frequencyPoints,1)} = 50
        r (1,1) {mustBeReal} = 3892.88
        c (1,1) {mustBeReal} = 2e-10
        N_range (1,2) = [1 10]
        L_range (1,2) = [0.1 10]
    end
    arguments (Output)
        theBestParametricFactors (1,2) cell
    end

    PCh_population = cell(populationSize, 2);
    % generate population of individuals (solutions)
    for row = 1:populationSize
        PCh_chromosome = PCh_generateParametricFactors(n,N_range,L_range); % generate chromosome
        PCh_chromosomeFitnessValue = FV_computeFitnessValue(E, CCh, CCh_B, PCh_chromosome, n, phaseRange, frequencyRange, frequencyPoints, r, c); % evaluate individual/chromosome inside population

        PCh_population{row,1} = PCh_chromosome;
        PCh_population{row,2} = PCh_chromosomeFitnessValue;
    end
%     disp('Population generated')
%     disp(PCh_population);

    % check if any solution satisfies the expected solutions quality
    % TODO: implement this check
    cumulativeFitnessSum = calculateCumulativeFitnessForRankSelection(populationSize);
%     disp('calculateCumulativeFitnessForRankSelection calculated')
    for i = 1:gaIterations
        %% selection - rank selection
        PCh_population = sortrows(PCh_population, 2, 'descend'); % sort rows based on the second column (fitness of individuals), descending highest first
        firstParentIndex = rouletteWheelSelection(cumulativeFitnessSum, populationSize);
        % select second individual (must be different from the first one)
        secondParentIndex = firstParentIndex;
        while firstParentIndex == secondParentIndex
            secondParentIndex = rouletteWheelSelection(cumulativeFitnessSum, populationSize);
        end
        %% crossover - one-point crossover
        [offspring1, offspring2] = crossoverParametricFactors(PCh_population, firstParentIndex, secondParentIndex);
        %% mutation - mutation of one element
        [offspring1, offspring2] = mutateOffsprings(offspring1, offspring2, n, N_range, L_range);
        %% replacement - add individuals to the population
        individualIndexForReplacement1 = randi([populationSize/2 populationSize],1);
        individualIndexForReplacement2 = randi([populationSize/2 populationSize],1);
        while individualIndexForReplacement1 == individualIndexForReplacement2
            individualIndexForReplacement2 = randi([populationSize/2 populationSize],1);
        end
        PCh_population{individualIndexForReplacement1,1} = offspring1; % replace random individual from the worse half
        PCh_population{individualIndexForReplacement1,2} = FV_computeFitnessValue(E,CCh, CCh_B, offspring1, n, phaseRange, frequencyRange, frequencyPoints, r, c);
        PCh_population{individualIndexForReplacement2,1} = offspring2; % replace random individual from the worse half
        PCh_population{individualIndexForReplacement2,2} = FV_computeFitnessValue(E,CCh, CCh_B, offspring2, n, phaseRange, frequencyRange, frequencyPoints, r, c);
        % check if offspring1 or offspring2 does not meet the predefined criteria
        % TODO: implement this check
    end
%     disp('Final population after iterations');
%     disp(PCh_population);

    % find the best individual in the population
    [theBestFitnessValue, indexOfBestIndividual] = max([PCh_population{:,2}]);
    theBestParametricFactors = cell(1,2);
    theBestParametricFactors{1,1} = PCh_population{indexOfBestIndividual,1};
    theBestParametricFactors{1,2} = PCh_population{indexOfBestIndividual,2};
%     disp(theBestParametricFactors);
%     disp(indexOfBestIndividual);
%     disp(PCh_population{indexOfBestIndividual,2});
end

function [offspring1_result, offspring2_result] = crossoverParametricFactors(PCh_population, firstParentIndex, secondParentIndex, crossoverType)
    arguments (Input)
        PCh_population (:,2) cell
        firstParentIndex (1,1) int32
        secondParentIndex (1,1) int32
        crossoverType {mustBeMember(crossoverType,["one-point","two-point","uniform"])} = 'one-point' % default to one-point crossover
    end
    arguments (Output)
        offspring1_result (2,:) {mustBeReal}
        offspring2_result (2,:) {mustBeReal}
    end
    firstParent = PCh_population{firstParentIndex,1}; % get NL parametric factors matrix
    secondParent = PCh_population{secondParentIndex,1}; % get NL parametric factors matrix

    parent1 = firstParent(2,:); % L parametric factor vector
    parent2 = secondParent(2,:); % L parametric factor vector
    
    % one-point crossover
    sizeOfLVector = numel(parent1);
    crossoverRandomPoint = randi(sizeOfLVector);
    if crossoverRandomPoint == 1 % fixing corner cases (if crossover point is the first or the last element
        crossoverRandomPoint = 2;
    elseif crossoverRandomPoint == sizeOfLVector
        crossoverRandomPoint = sizeOfLVector - 1;
    end
  
    offspring1 = [parent1(1:crossoverRandomPoint) parent2(crossoverRandomPoint+1:sizeOfLVector)];
    offspring2 = [parent2(1:crossoverRandomPoint) parent1(crossoverRandomPoint+1:sizeOfLVector)];

    offspring1_result = [firstParent(1,:); offspring1]; % return the matrix with N (unchanged) and modified L (PCh)
    offspring2_result = [secondParent(1,:); offspring2]; % return the matrix with N (unchanged) and modified L (PCh)
end

function [offspring1_result, offspring2_result] = mutateOffsprings(NL_offspring1, NL_offspring2, n, N_range, L_range)
    arguments (Input)
        NL_offspring1 (2,:) {mustBeReal}
        NL_offspring2 (2,:) {mustBeReal}
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)}
        N_range (1,2) = [1 10]
        L_range (1,2) = [0.1 10]
    end
    arguments (Output)
        offspring1_result (2,:) {mustBeReal}
        offspring2_result (2,:) {mustBeReal}
    end
    L_low = L_range(1,1);
    L_high = L_range(1,2);
    randomNumberForOffspring1 = L_low + (L_high - L_low) * rand(1);
    randomNumberForOffspring2 = L_low + (L_high - L_low) * rand(1);

    offspring1 = NL_offspring1(2,:); % get L parametric factor vector
    offspring2 = NL_offspring2(2,:); % get L parametric factor vector

    indexToMutate = randi(numel(offspring1));
    offspring1(indexToMutate) = randomNumberForOffspring1;

    indexToMutate = randi(numel(offspring2));
    offspring2(indexToMutate) = randomNumberForOffspring2;

    % N offspring1
    N_low = N_range(1,1);
    N_high = N_range(1,2);
    N_value = N_low + (N_high - N_low) * rand(1, 1);
    N_offspring1 = zeros(1,n) + N_value;
    
    % N offspring2
    N_value = N_low + (N_high - N_low) * rand(1, 1);
    N_offspring2 = zeros(1,n) + N_value;
    
    % mutate N only with some specific probability
    prob = 10;
    mutationProbabilityOfN = randi(prob);
    if mutationProbabilityOfN == prob
        offspring1_result = [N_offspring1; offspring1]; % return the matrix with N (unchanged) and modified L (PCh)
        offspring2_result = [N_offspring2; offspring2]; % return the matrix with N (unchanged) and modified L (PCh)
    else
        offspring1_result = [NL_offspring1(1,:); offspring1]; % return the matrix with N (unchanged) and modified L (PCh)
        offspring2_result = [NL_offspring2(1,:); offspring2]; % return the matrix with N (unchanged) and modified L (PCh)
    end
end
