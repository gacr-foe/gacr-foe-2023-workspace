function E = E_interconnectionsOfFourTerminalCoincidenceMatrixFactory()
    arguments (Output)
        E (2,4) cell
    end
    % Valid interconnections of adjacent four-terminal R-C-NR structures and their respective coincidence matrices
    C11 = [1 1 1 1; 1 1 1 1; 1 1 1 1; 1 1 1 1];
    C12 = [1 1 1 0; 1 1 1 0; 1 1 1 0; 0 0 0 1];
    C13 = [1 0 1 1; 0 1 0 0; 1 0 1 1; 1 0 1 1];
    C14 = [1 0 1 0; 0 1 0 0; 1 0 1 0; 0 0 0 1];
    C21 = [1 0 0 0; 0 1 1 1; 0 1 1 1; 0 1 1 1];
    C22 = [1 0 0 0; 0 1 0 1; 0 0 1 0; 0 1 0 1];
    C23 = [1 0 1 0; 0 1 0 1; 1 0 1 0; 0 1 0 1];
    C24 = [1 1 0 1; 1 1 0 1; 0 0 1 0; 1 1 0 1];
    % matrix with two rows and four columns, the element is retrieved using, e.g., E{1,2} to fetch first row and second column
    E = {C11, C12, C13, C14; C21, C22, C23, C24};
end