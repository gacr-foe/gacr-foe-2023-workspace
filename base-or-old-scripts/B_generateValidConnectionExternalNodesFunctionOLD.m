function B = B_generateValidConnectionExternalNodesFunctionOLD(m,n) % m = rows; n = columns;
    arguments (Input)
        m (1,1) int32 {mustBeGreaterThanOrEqual(m,4)} = 4  % the number of rows equals to the number of possible states of external nodes (in, gnd, float, con)
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4  % the number of columns corresponds to the number of external nodes of EFI (which is always 4)
    end
    arguments (Output)
        B {mustBeMember(B,[0 1])}
    end
    B = zeros(m,n);
    % below the "repair operations" are applied to provide valid external EFI switching scheme

    % Rule 1: there must be one to three "in" nodes
    addIn = randi(3); % one to three "in" nodes allowed
    for column = 1 : addIn
        B(1,randi(n)) = 1; % randomly add one "in" node (might replace "1" with "1" which I do not see as an issue)
    end

    % Rule 2: there must be at least one "gnd" node
    zeroInIndexes = find(~B(1,:)); % find zero indexes of "in" node
    zeroIndexesCount = numel(zeroInIndexes);
    B(2,zeroInIndexes(randi(zeroIndexesCount))) = 1; % fill-up "gnd" node on column with zero "in" node

    % Rule 3: in the remaining columns (zero columns) there must be "con" or "float" nodes
    % Rule 4: if the remaining columns (zero columns) are equal to just one column, add "float" node
    countZeroColumns = 0;
    indexOfZeroColumn = 0; %% should always be replaced or not used within IF statement
    for col=1:m
        sumColumnValues = sum(B(:,col));
        if sumColumnValues == 0
            countZeroColumns = countZeroColumns+1;
            indexOfZeroColumn = col;
        end
    end
    if countZeroColumns == 1
        B(3,indexOfZeroColumn) = 1; %% apply Rule 4
    else
        floatOrConn = randi([3 4],1);
        for col=1:m
            sumColumnValues = sum(B(:,col));
            if sumColumnValues == 0
                B(floatOrConn, col) = 1; % apply Rule 3
            end
        end
    end

    % Rule 5: check if each column contains exactly one element ("1")
    % this is automatically achieved by the previous rules
end
