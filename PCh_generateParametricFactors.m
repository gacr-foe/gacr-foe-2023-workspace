function PCh = PCh_generateParametricFactors(n,N_range, L_range)
    arguments (Input)
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4; % number of nodes in the solution
        N_range (1,2) = [1 10]
        L_range (1,2) = [0.1 10]
    end
    arguments (Output)
        PCh (2,:) {mustBeReal}
    end
    N_low = N_range(1,1);
    N_high = N_range(1,2);
    N_value = N_low + (N_high - N_low) * rand(1, 1);
    N = zeros(1,n) + N_value;

    L_low = L_range(1,1);
    L_high = L_range(1,2);
    L = L_low + (L_high - L_low) * rand(1, n);
    PCh = [N; L];
end