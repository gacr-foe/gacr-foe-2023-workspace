function GA_CCh = GA_CCh_geneticAlgorithmForCodingFactors(gaIterations, populationSize, selectionType, crossoverType, n, phaseRange, frequencyRange, frequencyPoints, r, c, N_range, L_range)
    arguments (Input)
        gaIterations (1,1) int32 {mustBeGreaterThanOrEqual(gaIterations,10)} = 250  % default value of iterations of genetic algorithm
        populationSize (1,1) int32 {mustBeGreaterThanOrEqual(populationSize,10)} = 50  % the number of individuals in the population
        selectionType {mustBeMember(selectionType,["rank","roulette","tournament"])} = 'rank' % default to rank selection
        crossoverType {mustBeMember(crossoverType,["one-point","two-point","uniform"])} = 'one-point' % default to one-point crossover
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4 % number of nodes in the solution
        phaseRange (1,2) = [33 37]
        frequencyRange (1,2) = [1e3 10e6] % frequency range should be between 1 kHz to 10 MHz
        frequencyPoints (1,1) int32 {mustBeGreaterThanOrEqual(frequencyPoints,1)} = 100
        r (1,1) {mustBeReal} = 3892.88 % resistance of top layer per unity normalized length L; zadávat jako reálný odpor na čtverec (sheet resistance) vrstvy na čipu (té s nižším R, aby N mohlo být >10)
        c (1,1) {mustBeReal} = 2e-10 % capacitance per unity normalized length L
        N_range (1,2) = [1 10]
        L_range (1,2) = [0.1 10]
    end

    E = E_interconnectionsOfFourTerminalCoincidenceMatrixFactory();

    CCh_population = cell(populationSize,4);
    for row=1:populationSize
        CCh_population{row,1} = CCh_generateConnectionFactorsChromosome(E,n);
        CCh_population{row,2} = B_generateValidConnectionExternalNodesFunction();
        CCh_population{row,3} = PCh_generateParametricFactors(n, N_range, L_range);
        CCh_population{row,4} = FV_computeFitnessValue(E,CCh_population{row,1},CCh_population{row,2},CCh_population{row,3},n,phaseRange,frequencyRange,frequencyPoints,r,c); % evaluate individual/chromosome inside population (including parametric factors) -- TODO: need to pass all necessary parameters
    end
    disp('Population generated')
    disp(CCh_population)

    cumulativeFitnessSum = calculateCumulativeFitnessForRankSelection(populationSize);

    % six cycles
    for x=1:6
        for i=1:gaIterations
            %% selection - rank selection
            CCh_population = sortrows(CCh_population, 4, 'descend'); % sort rows based on the third column (fitness value of individuals), descending highest first
            firstParentIndex = rouletteWheelSelection(cumulativeFitnessSum, populationSize);
            % select second individual (must be different from the first one)
            secondParentIndex = firstParentIndex;
            while firstParentIndex == secondParentIndex
                secondParentIndex = rouletteWheelSelection(cumulativeFitnessSum, populationSize);
            end
            %% crossover - one-point crossover
            [offspring1, offspring2] = crossoverCodingFactors(CCh_population, firstParentIndex, secondParentIndex);
            %% mutation - mutation of one element
            [offspring1, offspring2] = mutateOffsprings(E, offspring1, offspring2, n);
            %% evaluate newly created individuals (offsprings)
            offspring1{1,4} = FV_computeFitnessValue(E,offspring1{1,1}, offspring1{1,2},offspring1{1,3},n,phaseRange,frequencyRange,frequencyPoints,r,c);
            offspring2{1,4} = FV_computeFitnessValue(E,offspring2{1,1}, offspring2{1,2},offspring2{1,3},n,phaseRange,frequencyRange,frequencyPoints,r,c);
            % run parameter optimization for offsprings
%             GA_PCh_offspring1 = GA_PCh_geneticAlgorithmForParametricFactors(E,offspring1{1,1}, offspring1{1,2}, gaIterations, populationSize, selectionType, crossoverType, n, phaseRange, frequencyRange, frequencyPoints, r, c); % return optimal parametric factors for the best coding factors
%             GA_PCh_offspring2 = GA_PCh_geneticAlgorithmForParametricFactors(E,offspring2{1,1}, offspring2{1,2}, gaIterations, populationSize, selectionType, crossoverType, n, phaseRange, frequencyRange, frequencyPoints, r, c); % return optimal parametric factors for the best coding factors
% 
%             offspring1{1,3} = GA_PCh_offspring1{1,1};
%             offspring1{1,4} = GA_PCh_offspring1{1,2};
% 
%             offspring2{1,3} = GA_PCh_offspring2{1,1};
%             offspring2{1,4} = GA_PCh_offspring2{1,2};

            %% replacement - add individuals to the population
            individualIndexForReplacement1 = randi([populationSize/2 populationSize],1);
            individualIndexForReplacement2 = randi([populationSize/2 populationSize],1);
            while individualIndexForReplacement1 == individualIndexForReplacement2
                individualIndexForReplacement2 = randi([populationSize/2 populationSize],1);
            end
            for column=1:4
                CCh_population{individualIndexForReplacement1,column} = offspring1{1,column};
            end
            for column=1:4
              CCh_population{individualIndexForReplacement2,column} = offspring2{1,column}; % replace random individual from the worse half          
            end

            % check if offspring1 or offspring2 does not meet the predefined criteria
            % TODO: implement this check
        end
%         disp('Final population after iterations');
%         disp(CCh_population);
    
        % find individual with fitness value not equal to frequency points and optimize its parameters (this is important to do not remove the solution that can be the best)
        CCh_population = sortrows(CCh_population, 4, 'descend');
        for row = 1 : populationSize
            if CCh_population{row,4} ~= frequencyPoints
                theBestCodingFactors = cell(1,4);
                for column=1:4
                    theBestCodingFactors{1,column} = CCh_population{row,column};
                end
        %         disp('The best coding factors')
        %         disp(theBestCodingFactors)
                GA_CCh_inIteration = theBestCodingFactors;
                
                % run genetic algorithm optimizing parametric factors for individual that has fitness value not equal to frequency points
                CCh = GA_CCh_inIteration{1,1}; % (E,A) pairs
                CCh_B = GA_CCh_inIteration{1,2}; % B valid connection external nodess
        %         disp('get results')
        %         disp(CCh);
        %         disp(CCh_B);
                GA_PCh = GA_PCh_geneticAlgorithmForParametricFactors(E,CCh, CCh_B, gaIterations, populationSize, selectionType, crossoverType, n, phaseRange, frequencyRange, frequencyPoints, r, c); % return optimal parametric factors for the best coding factors
        %         disp('Parametric factors optimized')
        %         disp(GA_PCh)
        %         % replace optimized parametric factors and fitness value for best coding factors
                if GA_PCh{1,2} > CCh_population{row,4}  % check if the newly optimized parametric factors are better than the old ones
                    CCh_population{row,3} = GA_PCh{1,1}; % replace parametric factors for the best individual
                    CCh_population{row,4} = GA_PCh{1,2}; % replace fitness value for the best individual
                end
                break;
            end
        end
    end
    disp('Final population after iterations')
    disp(CCh_population)

    [theBestFitnessValue, indexOfSolution] = max([CCh_population{:,4}]);
    theBestSolution = cell(1,4);
    for column=1:4
        theBestSolution{1,column} = CCh_population{indexOfSolution,column};
    end
    disp('The best solution')
    disp(theBestSolution)
    GA_CCh = theBestSolution;
    disp('Connection factors')
    connectionFactors = GA_CCh{1,1};
    disp(connectionFactors)
    disp('Connection factors details')
    disp('E1')
    disp(connectionFactors{1,1})
    disp('A1')
    disp(connectionFactors{1,2})
    disp('E2')
    disp(connectionFactors{2,1})
    disp('A2')
    disp(connectionFactors{2,2})
    disp('E3')
    disp(connectionFactors{3,1})
    disp('A3')
    disp(connectionFactors{3,2})
    disp('B matrix')
    disp(GA_CCh{1,2})
    disp('Parametric factors')
    disp(GA_CCh{1,3})
    disp('Fitness value')
    disp(GA_CCh{1,4})

    % call once again fitness function to show diagrams
    FV_showDiagrams(E,GA_CCh{1,1},GA_CCh{1,2},GA_CCh{1,3},n,phaseRange,frequencyRange,frequencyPoints,r,c);
end

function [offspring1Result, offspring2Result] = crossoverCodingFactors(CCh_population, firstParentIndex, secondParentIndex, crossoverType, n)
    arguments (Input)
        CCh_population (:,4) cell
        firstParentIndex (1,1) int32
        secondParentIndex (1,1) int32
        crossoverType {mustBeMember(crossoverType,["one-point","two-point","uniform"])} = 'one-point' % default to one-point crossover
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4; % default value (might be higher)
    end
    arguments (Output)
        offspring1Result (1,4) cell
        offspring2Result (1,4) cell
    end
    firstParent = CCh_population{firstParentIndex,1}; % get connection factors set of [(E1,A1),(E2,A2),...]
    secondParent = CCh_population{secondParentIndex,1}; % get connection factors set of [(E1,A1),(E2,A2),...]
    
%     disp('parents [(E1,A1),(E2,A2),...] pairs')
    connectionFactorPairsSize = n-1; % get number of rows (number of connection factors pairs)

    offspring1Result = cell(1,4);
    offspring2Result = cell(1,4);

    offspring1ConnectionFactorsPairs = cell(connectionFactorPairsSize,2);
    offspring2ConnectionFactorsPairs = cell(connectionFactorPairsSize,2);

    %% crossover of order of connection factor pairs
    crossoverRandomPoint = randi([1,connectionFactorPairsSize],1);
    if crossoverRandomPoint == 1 % fixing corner cases (if crossover point is the first or the last element
        crossoverRandomPoint = 2;
    elseif crossoverRandomPoint == connectionFactorPairsSize
        crossoverRandomPoint = connectionFactorPairsSize - 1;
    end
    for row=1:crossoverRandomPoint
        offspring1ConnectionFactorsPairs{row,1} = firstParent{row,1};
        offspring1ConnectionFactorsPairs{row,2} = firstParent{row,2};
        offspring2ConnectionFactorsPairs{row,1} = secondParent{row,1};
        offspring2ConnectionFactorsPairs{row,2} = secondParent{row,2};
    end
    for row=(crossoverRandomPoint+1):connectionFactorPairsSize
        offspring1ConnectionFactorsPairs{row,1} = secondParent{row,1};
        offspring1ConnectionFactorsPairs{row,2} = secondParent{row,2};
        offspring2ConnectionFactorsPairs{row,1} = firstParent{row,1};
        offspring2ConnectionFactorsPairs{row,2} = firstParent{row,2};
    end

    offspring1Result{1,1} = offspring1ConnectionFactorsPairs;
    offspring1Result{1,2} = CCh_population{firstParentIndex,2}; % transfer B (valid connection external nodes function) from parent
    offspring1Result{1,3} = CCh_population{firstParentIndex,3}; % transfer PCh (parametric factors) from parent

    offspring2Result{1,1} = offspring2ConnectionFactorsPairs;
    offspring2Result{1,2} = CCh_population{secondParentIndex,2}; % transfer B (valid connection external nodes function) from parent
    offspring2Result{1,3} = CCh_population{secondParentIndex,3}; % transfer PCh (parametric factors) from parent
end

function [offspring1, offspring2] = mutateOffsprings(E, offspring1, offspring2, n)
    arguments (Input)
        E (2,4) cell
        offspring1 (1,4) cell
        offspring2 (1,4) cell
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} = 4; % default value (might be higher)
    end
    arguments (Output)
        offspring1 (1,4) cell
        offspring2 (1,4) cell
    end
    connectionFactorPairsSize = n-1;
   
    % mutate offspring 1
    randomConnectionFactorPair = randi(connectionFactorPairsSize);
    offspring1ConnectionFactorsPairs = offspring1{1,1}; % (E1,A1;E2,A2;E3,A3) pairs
    A_offspring1 = A_generateTerminalGroundingFunction(E,offspring1ConnectionFactorsPairs{randomConnectionFactorPair,1},n); % (instead of mutation the new A is generated for random E)
    offspring1ConnectionFactorsPairs{randomConnectionFactorPair,2} = A_offspring1;
    offspring1{1,1} = offspring1ConnectionFactorsPairs; % randomly selected A is mutated (replaced) with the new A -> with respect to the E for that it was performed
    offspring1{1,2} = B_generateValidConnectionExternalNodesFunction; %% added if it would improve something or not
    % mutate offspring 2
    randomConnectionFactorPair = randi(connectionFactorPairsSize);
    offspring2ConnectionFactorsPairs = offspring2{1,1};
    A_offspring2 = A_generateTerminalGroundingFunction(E,offspring2ConnectionFactorsPairs{randomConnectionFactorPair,1},n); % (instead of mutation the new A is generated (the probability of generating same A is low)
    offspring2ConnectionFactorsPairs{randomConnectionFactorPair,2} = A_offspring2;
    offspring2{1,1} = offspring2ConnectionFactorsPairs; % randomly selected A is mutated (replaced) with the new A
    offspring2{1,2} = B_generateValidConnectionExternalNodesFunction; %% added if it would improve something or not
end
