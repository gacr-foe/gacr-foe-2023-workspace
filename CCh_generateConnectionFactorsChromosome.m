function CCh = CCh_generateConnectionFactorsChromosome(E,n)
    arguments (Input)
        E (2,4) cell
        n (1,1) int32 {mustBeGreaterThanOrEqual(n,4)} % number of nodes in the solution
    end
    arguments (Output)
        CCh (:,2) cell
    end
    % E is list of valid interconnections of adjacent four-terminal R-C-NR structures and their respective coincidence matrix
    % A is the terminal grounding of particular interconnection (Ek)
    CCh = cell(n-1,2); % connection factors are between nodes, so it will always be (n-1)
    numberConnections = 4;
    for row=1:n-1
        Ek = E{randi(2),randi(numberConnections)};
        CCh{row,1} = Ek;
        CCh{row,2} = A_generateTerminalGroundingFunction(E,Ek,numberConnections);
    end
end