function cumulativeFitness = calculateCumulativeFitnessForRankSelection(populationSize)
    arguments (Input)
        populationSize (1,1) int32 {mustBeGreaterThanOrEqual(populationSize,10)}
    end
    arguments (Output)
        cumulativeFitness (1,1) int32 {mustBeGreaterThanOrEqual(cumulativeFitness,0)}
    end
    sum = 0;
    count = 0;
    while count <= populationSize
       sum = sum + count;
       count = count + 1;
    end
    cumulativeFitness = sum;
end
